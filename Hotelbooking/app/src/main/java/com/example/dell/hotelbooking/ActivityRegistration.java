package com.example.dell.hotelbooking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActivityRegistration extends AppCompatActivity {
    EditText rusername, rpassword, remail, rretypepassword;
    Button register;
    TextView alreadymember;
    boolean uflag=false,eflag=false,pflag=false,rflag=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        rusername = (EditText) findViewById(R.id.Rusername);
        remail = (EditText) findViewById(R.id.Remail);
        rpassword = (EditText) findViewById(R.id.Rpassword);
//        rretypepassword = (EditText) findViewById(R.id.Rretypepassword);
        register = (Button) findViewById(R.id.register);
        alreadymember= (TextView) findViewById(R.id.alreadymember);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateUsername(rusername.getText().toString())) {
                    uflag = true;
                }
                else{
                    uflag=false;
                    rusername.setError("Invalid username");
                }
                if (validateEmail(remail.getText().toString())) {
                    eflag = true;
                }
                else
                {
                    eflag=false;
                    remail.setError("Incorrect pattern");
                }
                if (validatePassword(rpassword.getText().toString()))
                {
                    pflag=true;
                }
                else {

                    pflag=false;
                    rpassword.setError("password should contain more than 9 characters");
                }
                if (validateReTypePassword(rretypepassword.getText().toString()))
                {
                    rflag=true;
                }
                else
                {
                    rflag=false;
                    rretypepassword.setError("Password Incorrect");
                }
                if(uflag&&eflag&&pflag&&rflag==true)
                {
                    Intent intent=new Intent(ActivityRegistration.this,ActivitySignIn.class);
                    startActivity(intent);
                }
            }
        });

        alreadymember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActivityRegistration.this,ActivitySignIn.class);
                startActivity(intent);
            }
        });
    }


    protected boolean validateUsername(String rusername) {
        if (rusername.isEmpty()) {
            return false;
        } else {
            return true;

        }

    }

    protected boolean validateEmail(String remail) {
        String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(remail);
        if (matcher.matches()) {
            return true;

        } else {
            return false;

        }
    }

    protected boolean validatePassword(String rpassword)

    {
        if (rpassword != null && rpassword.length() > 6) {
            return true;
        } else {
            return false;
        }
    }


    protected boolean validateReTypePassword(String rretypepassword)

    {
        if (rretypepassword.equals(rpassword.getText().toString())) {

            return true;
        } else {
            return false;

        }
    }


}
