package com.example.dell.hotelbooking;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

@RequiresApi(api = Build.VERSION_CODES.N)
public class ActivityHotelReservation extends AppCompatActivity {
    EditText checkin;
    EditText checkout;
    Button checkavailability;
    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_reservation);

        checkin = (EditText) findViewById(R.id.checkin_date);
        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, 0);
                DatePickerDialog datePickerDialog = new DatePickerDialog(ActivityHotelReservation.this, date_from, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePickerDialog.show();
//                // TODO Auto-generated method stub
//
            }
        });
        checkout = (EditText) findViewById(R.id.checkout_date);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub\

//
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE,1 );

//Set yesterday time milliseconds as date pickers minimum date
                DatePickerDialog datePickerDialog = new DatePickerDialog(ActivityHotelReservation.this, date_to, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });
        checkavailability = (Button) findViewById(R.id.checkavailability);
        checkavailability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkin.getText().toString().length()!=0&&checkout.getText().toString().length()!=0) {
                    Intent intent = new Intent(ActivityHotelReservation.this, ActivityAvailableRoom.class);
                    startActivity(intent);


                }
                else
                {

                    Toast.makeText(ActivityHotelReservation.this, "Select the date first", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    DatePickerDialog.OnDateSetListener date_from = new DatePickerDialog.OnDateSetListener() {//DatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabelFrom();
        }
    };
    DatePickerDialog.OnDateSetListener date_to = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabelTo();
        }
    };


    private void updateLabelFrom() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        checkin.setText(sdf.format(myCalendar.getTime()));
//        checkout.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateLabelTo() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        checkout.setText(sdf.format(myCalendar.getTime()));
//        checkout.setText(sdf.format(myCalendar.getTime()));
    }
}