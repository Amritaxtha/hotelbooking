package com.example.dell.hotelbooking;

import android.support.annotation.NonNull;
//import android.support.desigBottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

public class ActivityBottomNavigation extends AppCompatActivity  implements BottomNavigationView.OnNavigationItemSelectedListener{

BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);
         bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
//        bottomNavigationView.enableAnimation(false);
//        bottomNavigationView.enableShiftingMode(false);
//        bottomNavigationView.setIconSize(24, 24);
//        bottomNavigationView.setTextSize(10);
           }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                item.setChecked(true);
                item.setTitle("Home");
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new FragmentMain()).commit();

                Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();

                break;


            case R.id.profile:
                Toast.makeText(this, "this is profile", Toast.LENGTH_SHORT).show();

            case R.id.profil:
                Toast.makeText(this, "yo", Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}
