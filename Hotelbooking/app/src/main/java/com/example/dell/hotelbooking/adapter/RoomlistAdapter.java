package com.example.dell.hotelbooking.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.example.dell.hotelbooking.model.Rooms;
import com.example.dell.hotelbooking.R;




public class RoomlistAdapter extends RecyclerView.Adapter<RoomlistAdapter.MyCustomViewHolder> {

    private List<Rooms> roomsList;

    public RoomlistAdapter(List<Rooms> roomsList) {

        this.roomsList = roomsList;
    }


    @Override
    public MyCustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_list, parent, false);
        return new MyCustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RoomlistAdapter.MyCustomViewHolder holder, int position) {
        final Rooms rooms = roomsList.get(position);
        holder.name.setText(rooms.getName());
        holder.roomimages.setImageResource(rooms.getRoomimages());
        holder.price.setText(rooms.getPrice());


    }

    @Override
    public int getItemCount() {
        return roomsList.size();
    }

    public class MyCustomViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView roomimages;
        private TextView price;
        LinearLayout room_list;


        public MyCustomViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            roomimages = (ImageView) itemView.findViewById(R.id.roomimages);
            price = (TextView) itemView.findViewById(R.id.price);
        }

    }
}
