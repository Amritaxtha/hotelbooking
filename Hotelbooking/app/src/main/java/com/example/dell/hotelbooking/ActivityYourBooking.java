package com.example.dell.hotelbooking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.hotelbooking.model.Rooms;

public class ActivityYourBooking extends AppCompatActivity {
    TextView text;
    Button confirm,cancel;
    private Rooms rooms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your__booking);
        Bundle bundle=getIntent().getExtras();
        rooms=bundle.getParcelable("key");
        String s=rooms.getName();
        text.setText(s);

        text= (TextView) findViewById(R.id.text);
        confirm= (Button) findViewById(R.id.Confirm);
        cancel= (Button) findViewById(R.id.Cancel);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ActivityYourBooking.this, "Your booking is successful.", Toast.LENGTH_SHORT).show();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActivityYourBooking.this,ActivitySelectRooms.class);
                startActivity(intent);
            }
        });
    }
}
