package com.example.dell.hotelbooking.adapter;

/*
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.dell.hotelbooking.ActivityBookNow;
import com.example.dell.hotelbooking.ActivityYourBooking;
import com.example.dell.hotelbooking.R;
import com.example.dell.hotelbooking.model.Book;
import com.example.dell.hotelbooking.model.Rooms;

import java.util.List;

*/
/**
 * Created by Acer on 7/12/2017.
 *//*


public class BookAdapter extends RecyclerView.Adapter<BookAdapter.MyCustomViewHolder> {
    private List<Rooms> roomsList;
    Spinner nrooms, nguest;
    String roominput, guestinput;
    TextView broom, bguest;
    private Context context;
    Button ok;

    public BookAdapter(List<Rooms> roomsList, Context context) {
        this.context = context;
        this.roomsList = roomsList;
    }


    @Override
    public BookAdapter.MyCustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_list, parent, false);
        return new BookAdapter.MyCustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookAdapter.MyCustomViewHolder holder, final int position) {
        final Rooms rooms = roomsList.get(position);
        holder.name.setText(rooms.getName());
        holder.roomimages.setImageResource(rooms.getRoomimages());
        holder.price.setText(rooms.getPrice());

        holder.room_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.activity_book_now);
                TextView name1 = (TextView) dialog.findViewById(R.id.dialog_name);
                final Rooms rooms = roomsList.get(position);
                name1.setText(rooms.getName());
                ImageView image1 = (ImageView) dialog.findViewById(R.id.roomimages);
                image1.setImageResource(rooms.getRoomimages());
                TextView price1 = (TextView) dialog.findViewById(R.id.price);
                price1.setText(rooms.getPrice());
                nrooms = (Spinner) dialog.findViewById(R.id.nrooms);
                nguest = (Spinner) dialog.findViewById(R.id.nguest);
                ok = (Button) dialog.findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        roominput = nrooms.getSelectedItem().toString();
                        guestinput = nguest.getSelectedItem().toString();
                        Intent intent = new Intent(context, ActivityYourBooking.class);
                        intent.putExtra("key", rooms);
                        v.getContext().startActivity(intent);
                    }
                });
                dialog.show();
                */
/*ok= (Button)findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nrooms= (Spinner) findViewById(R.id.nrooms);
                        nguest= (Spinner) findViewById(R.id.nguest);
                        roominput=nrooms.getSelectedItem().toString();
                        guestinput=nguest.getSelectedItem().toString();
                        Intent intent=new Intent(ActivityBookNow.this, ActivityYourBooking.class);
                        Bundle bundle=new Bundle();
                        bundle.putParcelable("key",book);
                        intent.putExtras(bundle);
                        v.getContext().startActivity(intent);*//*

            }
        });


    }


    @Override
    public int getItemCount() {
        return roomsList.size();
    }

    public class MyCustomViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView roomimages;
        private TextView price;
        LinearLayout room_list;


        public MyCustomViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            roomimages = (ImageView) itemView.findViewById(R.id.roomimages);
            price = (TextView) itemView.findViewById(R.id.price);
            room_list = (LinearLayout) itemView.findViewById(R.id.room_list);
        }

    }

}
*/

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.dell.hotelbooking.ActivityYourBooking;
import com.example.dell.hotelbooking.R;

import com.example.dell.hotelbooking.model.Book;
import com.example.dell.hotelbooking.model.Rooms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Dell on 12/07/2017.
 */

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.MyCustomViewHolder> {

    private List<Rooms> roomsList;
    Spinner nrooms, nguest;
    String roominput, guestinput;
    TextView broom, bguest;
    private Context context;
    Button ok;
    private Book book;
    SharedPreferences sharedpreferences;
    Bundle bundle;
    HashMap<String, List<Book>> bookRoom;
    LinkedHashMap<String, Book> hashMap;

    public BookAdapter(List<Rooms> roomsList, Context context) {
        this.context = context;
        this.roomsList = roomsList;
        bookRoom=new HashMap<>();
        hashMap=new LinkedHashMap<String, Book>();
    }


    @Override
    public BookAdapter.MyCustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_list, parent, false);
        return new BookAdapter.MyCustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BookAdapter.MyCustomViewHolder holder, final int position) {
        final Rooms rooms = roomsList.get(position);
        holder.name.setText(rooms.getName());
        holder.roomimages.setImageResource(rooms.getRoomimages());
        holder.price.setText(rooms.getPrice());

        holder.room_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.activity_book_now);
                TextView name1 = (TextView) dialog.findViewById(R.id.dialog_name);
                final Rooms rooms = roomsList.get(position);
                name1.setText(rooms.getName());

                ImageView image1 = (ImageView) dialog.findViewById(R.id.roomimages);
                image1.setImageResource(rooms.getRoomimages());
                TextView price1 = (TextView) dialog.findViewById(R.id.price);
                price1.setText(rooms.getPrice());
                final TextView textView= (TextView) dialog.findViewById(R.id.idText);
                textView.setText(rooms.getId());
                nrooms = (Spinner) dialog.findViewById(R.id.nrooms);
                nguest = (Spinner) dialog.findViewById(R.id.nguest);
                ok = (Button) dialog.findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String a = nrooms.getSelectedItem().toString();
                        String b = nguest.getSelectedItem().toString();
//                        List<Book> books = new ArrayList<Book>();
                        Book books=new Book(textView.getText().toString(),a, b);
//                        books.add(new Book(textView.getText().toString(),a, b));
//                        book = books.get(position);
//                        bookRoom.put(textView.getText().toString(),books);
                        hashMap.put(textView.getText().toString(),books);
                        bundle = new Bundle();
//                        Intent i = new Intent(context, ActivityYourBooking.class);
//                        bundle.putParcelable("Value", book);
//                        i.putExtras(bundle);


                        dialog.dismiss();

//                        context.startActivity(i);
                    }
                });
                dialog.show();

//
// initialization stuff, blah blah

            }
        });


    }


    @Override
    public int getItemCount() {
        return roomsList.size();
    }

    public class MyCustomViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView roomimages;
        private TextView price;
        LinearLayout room_list;


        public MyCustomViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            roomimages = (ImageView) itemView.findViewById(R.id.roomimages);
            price = (TextView) itemView.findViewById(R.id.price);
            room_list = (LinearLayout) itemView.findViewById(R.id.room_list);
        }

    }

    public LinkedHashMap<String, Book> getBookedRoom(){
        return hashMap;
    }

}

