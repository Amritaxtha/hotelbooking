package com.example.dell.hotelbooking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.example.dell.hotelbooking.adapter.RoomlistAdapter;
import com.example.dell.hotelbooking.model.Rooms;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.example.dell.hotelbooking.adapter.RoomlistAdapter;
import com.example.dell.hotelbooking.model.Rooms;

import java.util.ArrayList;
import java.util.List;

public class ActivityAvailableRoom extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RoomlistAdapter adapter;
    private List<Rooms> roomlist;
    Button signinfirst;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_room);
        roomlist=new ArrayList<>();
        recyclerView= (RecyclerView) findViewById(R.id.recyclerview);
        adapter=new RoomlistAdapter(roomlist);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(ActivityAvailableRoom.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        roomlistExtract();
        signinfirst= (Button) findViewById(R.id.siginfirst);
        signinfirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActivityAvailableRoom.this,ActivitySignIn.class);
                startActivity(intent);
                finish();
            }
        });



    }
    private void roomlistExtract()

    {
        Rooms rooms = new Rooms("s", "Single Room", R.drawable.singleroom, "2500/night");
        roomlist.add(rooms);

        rooms = new Rooms("d", "Double Room", R.drawable.doubleroom, "3000/night");
        roomlist.add(rooms);

        rooms = new Rooms("t", "Twin Room", R.drawable.twinroom, "3000/night");
        roomlist.add(rooms);

        rooms = new Rooms("td", "Twin Double Room", R.drawable.twindouble, "4000/night");
        roomlist.add(rooms);

        rooms = new Rooms("h", "Hollywood twin Room", R.drawable.hollywoodtwin, "4500/night");
        roomlist.add(rooms);

        adapter.notifyDataSetChanged();

    }

}

