package com.example.dell.hotelbooking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.dell.hotelbooking.adapter.BookAdapter;
import com.example.dell.hotelbooking.model.Book;
import com.example.dell.hotelbooking.model.Rooms;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Acer on 7/13/2017.
 */



/*
public  class ActivitySelectRooms extends AppCompatActivity {

    private RecyclerView recyclerView;
    private BookAdapter adapter;
    private List<Rooms> roomlist;
    Toolbar toolbar;
    Button booknow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_rooms);
        roomlist=new ArrayList<>();
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        recyclerView= (RecyclerView) findViewById(R.id.recyclerview);
        adapter=new BookAdapter(roomlist,this);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(ActivitySelectRooms.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        booknow= (Button) findViewById(R.id.booknow);
        booknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActivitySelectRooms.this, ActivityYourBooking.class);
                startActivity(intent);
            }
        });


        roomlistExtract();

    }
    private void roomlistExtract()

    {
        Rooms rooms=new Rooms("Single Room", R.drawable.singleroom,"2500/night");
        roomlist.add(rooms);

        rooms=new Rooms("Double Room",R.drawable.doubleroom,"3000/night");
        roomlist.add(rooms);

        rooms=new Rooms("Twin Room",R.drawable.twinroom,"3000/night");
        roomlist.add(rooms);

        rooms=new Rooms("Twin Double Room",R.drawable.twindouble,"4000/night");
        roomlist.add(rooms);

        rooms=new Rooms("Hollywood twin Room",R.drawable.hollywoodtwin,"4500/night");
        roomlist.add(rooms);

        adapter.notifyDataSetChanged();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return super.onCreateOptionsMenu(menu);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.logout:
                Intent intent=new Intent(ActivitySelectRooms.this,MainActivity.class);
                startActivity(intent);
        }



        return super.onOptionsItemSelected(item);
    }

}
*/
public class ActivitySelectRooms extends AppCompatActivity {

    private RecyclerView recyclerView;
    private BookAdapter adapter;
    private List<Rooms> roomlist;
    Toolbar toolbar;
    Button booknow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_rooms);
        roomlist = new ArrayList<>();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        adapter = new BookAdapter(roomlist, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ActivitySelectRooms.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        booknow = (Button) findViewById(R.id.booknow);
        booknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(ActivitySelectRooms.this, ActivityYourBooking.class);

//                startActivity(intent);

                LinkedHashMap<String, Book> books = adapter.getBookedRoom();
                Book books1;
                for (Map.Entry<String,Book> entry  : books.entrySet()) {
//                    System.out.println("values:: "+entry.getValue().toString());
                    System.out.println("key:: "+entry.getKey());
                    books1=entry.getValue();
                    System.out.println("values:: "+books1.getNumberroom()+"values:: "+books1.getNumberguest());


//                    System.out.println("value:: " + value.getKey()+"values:: "+value.getValue().toString());
                }
            }
        });
        roomlistExtract();
    }

    private void roomlistExtract() {
        Rooms rooms = new Rooms("s", "Single Room", R.drawable.singleroom, "2500/night");
        roomlist.add(rooms);

        rooms = new Rooms("d", "Double Room", R.drawable.doubleroom, "3000/night");
        roomlist.add(rooms);

        rooms = new Rooms("t", "Twin Room", R.drawable.twinroom, "3000/night");
        roomlist.add(rooms);

        rooms = new Rooms("td", "Twin Double Room", R.drawable.twindouble, "4000/night");
        roomlist.add(rooms);

        rooms = new Rooms("h", "Hollywood twin Room", R.drawable.hollywoodtwin, "4500/night");
        roomlist.add(rooms);

        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.logout:
                Intent intent = new Intent(ActivitySelectRooms.this, MainActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}

