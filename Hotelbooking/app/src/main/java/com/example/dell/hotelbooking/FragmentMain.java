package com.example.dell.hotelbooking;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.dell.hotelbooking.adapter.DashboardListAdapter;
import com.example.dell.hotelbooking.adapter.ImageSliderAdapter;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class FragmentMain extends Fragment {
    private static ViewPager mPager;
    private static int currentPage = 0;
    TabLayout tabLayout;
    ImageView imageView;
    ListView list;
    String[] web = {
            "Reservation",
            "Hotel Information",
            "Services",
            "Contact Us"
    } ;
    Integer[] imageId = {
            R.drawable.ic_action_name,
            R.drawable.ic_action_name,
            R.drawable.ic_action_name,
            R.drawable.ic_action_name,


    };
    private static final Integer[] XMEN = {R.drawable.hotel1, R.drawable.hotel, R.drawable.hotel3, R.drawable.hotel5, R.drawable.hotel6};
    private ArrayList<Integer> XMENArray = new ArrayList<Integer>();
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main,container,false);
        return  view;
         }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();

        DashboardListAdapter adapter = new DashboardListAdapter(getActivity(), web, imageId);
        list=(ListView)view.findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                int itemPosition = position;

                //Listview clicked item value
                String itemValue = (String)list.getItemAtPosition(position);
                switch(itemPosition) {

                    case 0:
                        Intent appInfo = new Intent(getActivity(), ActivityHotelReservation.class);
                        startActivity(appInfo);
                        break;
                    case 1:
                        Intent appInfo1 = new Intent(getActivity(), ActivityRegistration.class);
                        startActivity(appInfo1);
                        break;
                    case 2:
                        Intent appInfo2 = new Intent(getActivity(),ActivityServices.class);
                        startActivity(appInfo2);

                        break;

                    case 3:
                        Intent appInfo3 = new Intent(getActivity(), ActivityContact.class);
                        startActivity(appInfo3);
                        break;
                }


            }
        });

    }
    private void init() {
        for (int i = 0; i < XMEN.length; i++)
            XMENArray.add(XMEN[i]);

        mPager = (ViewPager) view.findViewById(R.id.pager);
        mPager.setAdapter(new ImageSliderAdapter(getActivity(), XMENArray));
        CircleIndicator indicator = (CircleIndicator) view.findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == XMEN.length) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);
    }
}