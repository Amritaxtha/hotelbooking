package com.example.dell.hotelbooking.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Acer on 7/9/2017.
 */

public class Rooms implements Parcelable {
    private String id;

    private String name;
    private int roomimages;
    private String price;

    public Rooms(String id, String name, int roomimages, String price) {
        this.id = id;
        this.name = name;
        this.roomimages = roomimages;
        this.price = price;
    }

    protected Rooms(Parcel in) {
        name = in.readString();
        roomimages = in.readInt();
        price = in.readString();
        id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(roomimages);
        dest.writeString(price);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Rooms> CREATOR = new Creator<Rooms>() {
        @Override
        public Rooms createFromParcel(Parcel in) {
            return new Rooms(in);
        }

        @Override
        public Rooms[] newArray(int size) {
            return new Rooms[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoomimages() {
        return roomimages;
    }

    public void setRoomimages(int roomimages) {
        this.roomimages = roomimages;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

