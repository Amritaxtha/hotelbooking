package com.example.dell.hotelbooking.model;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dell on 13/07/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Dell on 13/07/2017.
 */

public class Book implements Parcelable {
    String numberroom;
    String numberguest;
    String id;


    public Book(String numberroom, String numberguest, String id) {
        this.numberroom = numberroom;
        this.numberguest = numberguest;
        this.id = id;
    }

    public Book(String numberroom, String numberguest) {
        this.numberroom = numberroom;
        this.numberguest = numberguest;
    }


    protected Book(Parcel in) {
        numberroom = in.readString();
        numberguest = in.readString();
        id = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(numberroom);
        dest.writeString(numberguest);
        dest.writeString(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Book> CREATOR = new Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) {
            return new Book(in);
        }

        @Override
        public Book[] newArray(int size) {
            return new Book[size];
        }
    };

    public String getNumberroom() {
        return numberroom;
    }

    public void setNumberroom(String numberroom) {
        this.numberroom = numberroom;
    }

    public String getNumberguest() {
        return numberguest;
    }

    public void setNumberguest(String numberguest) {
        this.numberguest = numberguest;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
